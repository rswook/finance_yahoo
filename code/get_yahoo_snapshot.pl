#!/usr/bin/perl -w
##############################################################
#
# Program:        get_yahoo_snapshot.pl
# Author:         Ryan Sowers
#
# Created on:     <2011-10-19 21:00:00>
# Time-stamp:     <2011-10-20 23:59:12>
#
# Purpose:        Download a current snapshot of yahoo data
#
# Usage:          get_yahoo_snapshot.pl
#
# Yahoo Data Items Available:
# 	s  = Symbol 
# 	e1 = Error Indication (returned for symbol changed / invalid) 
# 	n  = Name 
# 	x  = Stock Exchange 
# 	l  = Last Trade (With Time) 
# 	l1 = Last Trade (Price Only) 
# 	d1 = Last Trade Date 
# 	k3 = Last Trade Size 
# 	t1 = Last Trade Time 
# 	o  = Open 
# 	p  = Previous Close 
# 	v  = Volume 
# 	a2 = Average Daily Volume 
# 	a  = Ask 
# 	b2 = Ask (Real-time) 
# 	a5 = Ask Size 
# 	b  = Bid 
# 	b3 = Bid (Real-time) 
# 	b6 = Bid Size 
# 	h  = Day's High 
# 	g  = Day's Low 
# 	m  = Day's Range 
# 	m2 = Day's Range (Real-time) 
# 	w1 = Day's Value Change 
# 	w4 = Day's Value Change (Real-time) 
# 	k  = 52-week High 
# 	j  = 52-week Low 
# 	w  = 52-week Range 
# 	m4 = 200-day Moving Average 
# 	m3 = 50-day Moving Average 
# 	g3 = Annualized Gain 
# 	c1 = Change 
# 	c  = Change & Percent Change 
# 	c6 = Change (Real-time) 
# 	m5 = Change From 200-day Moving Average 
# 	m7 = Change From 50-day Moving Average 
# 	k4 = Change From 52-week High 
# 	j5 = Change From 52-week Low 
# 	p2 = Change in Percent 
# 	k2 = Change Percent (Real-time) 
# 	k5 = Percebt Change From 52-week High 
# 	m6 = Percent Change From 200-day Moving Average 
# 	m8 = Percent Change From 50-day Moving Average 
# 	j6 = Percent Change From 52-week Low 
# 	j1 = Market Capitalization 
# 	j3 = Market Cap (Real-time) 
# 	r1 = Dividend Pay Date 
# 	y  = Dividend Yield 
# 	d  = Dividend/Share 
# 	q  = Ex-Dividend Date 
# 	e  = Earnings/Share 
# 	j4 = EBITDA 
# 	e7 = EPS Estimate Current Year 
# 	e9 = EPS Estimate Next Quarter 
# 	e8 = EPS Estimate Next Year 
# 	b4 = Book Value 
# 	p6 = Price/Book 
# 	r6 = Price/EPS Estimate Current Year 
# 	r7 = Price/EPS Estimate Next Year 
# 	p5 = Price/Sales 
# 	r  = P/E Ratio 
# 	r2 = P/E Ratio (Real-time) 
# 	r5 = PEG Ratio 
# 	f6 = Float Shares 
# 	t8 = 1 yr Target Price 
# 	s7 = Short Ratio 
# 	t7 = Ticker Trend
#
# Untested Yahoo Data Items:
# 	c3 = Commission
# 	g1 = Holdings Gain Percent
# 	g4 = Holdings Gain
# 	g5 = Holdings Gain Percent (Real-time)
# 	i  = More Info
# 	i5 = Order Book (Real-time)
# 	l3 = Low Limit
# 	n4 = Notes
# 	p1 = Price Paid
# 	s1 = Shares Owned
# 	t6 = Trade Links
# 	v7 = Holdings Value (Real-time)
# 	c8 = After Hours Change (Real-time)
# 	d2 = Trade Date
# 	g6 = Holdings Gain (Real-time)
# 	l2 = High Limit
# 	v1 = Holdings Value
#
##############################################################

use strict;
use Getopt::Std;

my $curl = "/usr/bin/curl";
my $yahoo_prefix = "http\:\/\/download\.finance\.yahoo\.com\/d\/quotes\.csv\?s\=";

########################################
# Read text file into an array
########################################
sub S_read_into_array {
    my ($infile) = @_;

    my ($line,@outarray);

    open(INFILE,"$infile") or die "Ticker File: Cannot open the input file\n";
    while(<INFILE>) {
	$line = $_;
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	push(@outarray,$line);
    }
    close(INFILE);

    return @outarray;
}




########################################
# Main program
########################################
MAIN: {

    my $pid = $$;

    my $ticker_file = "/home/rsowers/dev/stock_price_downloader/all_tickers.txt";
    my $yahoo_data_file = "/home/rsowers/dev/stock_price_downloader/yahoo_data_for_testing.txt";

    #####################################################
    # Let's figure out which tickers yahoo populates
    #####################################################

    my @raw_tickers = S_read_into_array($ticker_file);
    my (@valid_tickers, @invalid_tickers, @changed_tickers);

    foreach my $ticker (@raw_tickers) {
	$ticker =~ s/^\s+//;
	$ticker =~ s/\s+$//;

	my $tmp_url  =  $yahoo_prefix . $ticker . "\&f\=se1\&e\=\.csv";
	my $tmp_file = "./tmp/.". $pid . "_tmp.txt";

	unlink($tmp_file) unless (!-f $tmp_file);

	system("$curl -s -A Mozilla \"$tmp_url\" > $tmp_file");
	
	open(TEMPFILE,"$tmp_file") or die "Cannot open $tmp_file\n";
	my $ticker_info = <TEMPFILE>;
	close(TEMPFILE);

	unlink($tmp_file) unless (!-f $tmp_file);

	$ticker_info =~ s/^\s+//;
	$ticker_info =~ s/\s+$//;
	$ticker_info =~ s/\"//g;

	my @ticker_values = split(",",$ticker_info);

	my $ticker_status = $ticker_values[1];

	if ($ticker_status =~ "N/A") {
	    push(@valid_tickers,$ticker);
	} elsif ($ticker_status =~ "No such ticker symbol.") {
	    push(@invalid_tickers,$ticker);
	} elsif ($ticker_status =~ "Ticker symbol has changed to" ) {
	    $ticker_status =~ s/Ticker symbol has changed to\: \<a href\=\/q\?s\=//;
	    my @ticker_changes = split(">",$ticker_status);
	    my $changed_ticker = $ticker_changes[0];
	    push(@changed_tickers,$changed_ticker);
	}
    }
    
    my @yahoo_tickers = sort (@valid_tickers,@changed_tickers);

    #####################################################
    # Open the output file
    #####################################################

    open(DATAFILE,">$yahoo_data_file") or die "Cannot open $yahoo_data_file\n";

    #####################################################
    # Read in the requested yahoo data items
    #####################################################

    my $yahoo_query_list = "/home/rsowers/dev/stock_price_downloader/yahoo_data_items.txt";
    
    my @query_list = S_read_into_array($yahoo_query_list);

    #####################################################
    # Get some data with the valid yahoo tickers
    #####################################################
    
    foreach my $query (@query_list) { 
	my @query_item = split(/\|/,$query);
	print DATAFILE "$query_item[1]|";
    }
    print DATAFILE "\n";

    my @raw_stock_data;
    
    foreach my $ticker (@yahoo_tickers) {
	$ticker =~ s/^\s+//;
	$ticker =~ s/\s+$//;
	
	my @ticker_data_items;

	foreach my $query (@query_list) {
	    my @query_item = split(/\|/,$query);
	    
	    my $request_code = $query_item[0];

	    my $tmp_url  =  $yahoo_prefix . $ticker . "\&f\=" . $request_code . "\&e\=\.csv";
	    my $tmp_file = "./tmp/.". $pid . "_tmp.txt";

	    unlink($tmp_file) unless (!-f $tmp_file);
	    
	    system("$curl -s -A Mozilla \"$tmp_url\" > $tmp_file");
	    
	    open(TEMPFILE,"$tmp_file") or die "Cannot open $tmp_file\n";
	    my $ticker_data = <TEMPFILE>;
	    close(TEMPFILE);
	    
	    $ticker_data =~ s/^\s+//;
	    $ticker_data =~ s/\s+$//;
	    $ticker_data =~ s/\"//g;
	    $ticker_data =~ s/\<b\>//;
	    $ticker_data =~ s/\<\/b\>//;
	    $ticker_data =~ s/ \- /-/;
	    $ticker_data =~ s/N\/A//g;
	    $ticker_data =~ s/\&nbsp\;//g;
	    $ticker_data =~ s/\|\-\|/\|\|/g;

	    push(@ticker_data_items,$ticker_data);
	    
	    unlink($tmp_file);
	}

	foreach my $data_item (@ticker_data_items) { print DATAFILE "$data_item|"; }
	print DATAFILE "\n";
     }

}
