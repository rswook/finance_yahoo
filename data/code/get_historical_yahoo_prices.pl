#!/usr/bin/perl -w
##############################################################
#
# Program:        get_historical_yahoo_prices.pl
# Author:         Ryan Sowers
#
# Created on:     <2011-07-28 21:25:32>
# Time-stamp:     <2011-08-03 22:15:29>
#
# Purpose:        Download historical prices from yahoo in csv
#                 format.  Combine into single file.
#
# Usage:          get_historical_yahoo_prices.pl 
#
# Example Runline: 
#
##############################################################

use strict;
use Getopt::Std;
use POSIX;

my $curl = "/usr/bin/curl";

my $url_prefix = "http\:\/\/ichart\.finance\.yahoo\.com\/table\.csv\?s\=";
my $url_suffix = "\&amp\;d\=12\&amp\;e\=31\&amp\;f\=2099\&amp\;g\=d\&amp\;a\=0\&amp\;b\=1\&amp\;c\=1950\&amp\;ignore\=\.csv";

########################################
# Usage
########################################
sub S_usage {
    my ($error_msg) = @_;
    
    print "\nUSAGE: $0 -l <logfile> -t <ticker file> -o <output path> -f <output file name>\n\n";
    print "$error_msg\n\n";
    exit(0);
}

########################################
# Check command line inputs
########################################
sub S_check_inputs {
    my($lfile,$tfile,$opath,$ofile) = @_;

    S_usage("Must specify a log file") unless 
	(defined $lfile);

    S_usage("Must specify a ticker file") unless
	(defined $tfile);

    S_usage("Ticker file does not exist") unless
	(-f $tfile);

    S_usage("Must specify the production directory") unless 
	(defined $opath);

    S_usage("The production directory specified does not exist") unless
	(-d $opath);

    S_usage("Must specify an output file name") unless
	(defined $ofile);

}

########################################
# Read text file into an array
########################################
sub S_read_into_array {
    my ($infile) = @_;

    my ($line,@outarray);

    open(INFILE,"$infile") or die "Ticker File: Cannot open the input file\n";
    while(<INFILE>) {
	$line = $_;
	chomp($line);
	push(@outarray,$line);
    }
    close(INFILE);

    return @outarray;
}


########################################
# Read text file into a string
########################################
sub S_read_into_string {
    my ($infile) = @_;

    my $file_string;

    open(INFILE,"$infile") or die "Cannot open the file for the string\n";
    binmode INFILE;
    while (<INFILE>) {
	$file_string .= $_;
    }
    close(INFILE);

    return $file_string;
}

########################################
# Get the headerline for the file
########################################
sub S_get_first_line {
    my ($infile) = @_;

    my ($out_string, $line_count);

    $line_count = 0;

    open(INFILE,"$infile") or die "Output file: Cannot open the file\n";
    while(<INFILE>) {
	$line_count++;
	if ($line_count == 1) {
	    $out_string = $_;
	    chomp($out_string);
	}
    }
    close(INFILE);

    return $out_string;
}


########################################
# Skip the header line
########################################
sub S_get_file_body {
    my ($ticker_string,$infile) = @_;

    my ($line,@outarray,$file_line_count);

    $file_line_count = 0;

    open(INFILE,"$infile") or die "Getting raw pricing data: Cannot open the input file\n";
    while(<INFILE>) {
	$file_line_count++;

	if ($file_line_count > 1) {
	    $line = $_;
	    chomp($line);
	    $line = $ticker_string . "," . $line;
	    push(@outarray,$line);
	}
    }
    close(INFILE);

    return @outarray;
}


########################################
# Main program
########################################
MAIN: {

    getopts('l:t:o:p:f:');
    our($opt_l, $opt_t, $opt_o, $opt_p, $opt_f);

    my $tmp_logfile = $opt_l;
    my $ticker_file = $opt_t;
    my $output_path = $opt_o;
    my $file_prefix = $opt_p;
    my $output_file = $opt_f;

    my $prod_date = POSIX::strftime('%Y%m%d',localtime);

    S_check_inputs($tmp_logfile,$ticker_file,$output_path,$output_file);

    ##### Add the production date to the log name
    $tmp_logfile =~ s/\.[^.]+$//;
    my $logfile = $tmp_logfile . "_" . $prod_date . ".log";

    ##### Log Header
    open(LOGFILE, ">$logfile") or die "Cannot open $logfile";

    print LOGFILE "****************************************\n";
    print LOGFILE "* Getting historical Yahoo prices\n";
    print LOGFILE "* Production date: $prod_date\n";
    print LOGFILE "****************************************\n\n";

    ##### Add start time to log
    my $starttime = localtime($^T);
    print LOGFILE "Start time: $starttime\n\n";

    my @tickers = S_read_into_array($ticker_file);

    my (@valid_tickers,@failed_tickers);
    
    print LOGFILE "NOTE:  Read in ticker file: $ticker_file\n\n";
    print LOGFILE "****************************************\n\n";

    my $ticker_count = 0;
    my $header_line;

    my $hist_price_file = $output_path . "/" . $output_file;
    
    open(PRICEFILE,">$hist_price_file") or die "Temp Price File: Cannot open output file $hist_price_file";

    ##### Get the raw data from 
    foreach my $ticker (@tickers) {
	$ticker_count++;

	my $file_url = $url_prefix . $ticker . $url_suffix;
	
	my $out_price_file = $output_path . "\/" . $ticker . "_prices.txt";
	
	if (-f $out_price_file) {
	    print LOGFILE "WARN: Price file for $ticker already exists - replacing\n";
	}
	
	print LOGFILE "NOTE:  Getting data for: $ticker\n";
	
	system("$curl -s -A Mozilla \"$file_url\" > $out_price_file");
	
	my $price_file_size = -s $out_price_file;

	my $file_test_string = S_read_into_string($out_price_file);

	if ($file_test_string =~ m/HTML/) {
	    print LOGFILE "ERROR: Invalid file - $out_price_file\n";
	    print LOGFILE "ERROR: File not included in output file\n\n";
	    push(@failed_tickers,$ticker);
	} else {
	    print LOGFILE "NOTE: filesize = $price_file_size\n\n";
	    push(@valid_tickers,$ticker);

	    my @price_lines = S_get_file_body($ticker,$out_price_file);
	    
	    foreach my $price_line (@price_lines) {
		print PRICEFILE "$price_line\n";
	    }	
	}
	unlink($out_price_file);
    }

    ##### Close out the price file
    close(PRICEFILE);

    ##### Write out valid and failed tickers to files
    my $valid_ticker_file = $output_path . "/valid_tickers.txt";
    my $failed_ticker_file = $output_path . "/failed_tickers.txt";

    open(VTICKERS,">$valid_ticker_file") or die "Coult not open $valid_ticker_file\n";
    foreach my $tmp_ticker (@valid_tickers) {
	print VTICKERS "$tmp_ticker\n";
    }
    close(VTICKERS);

    open(FTICKERS,">$failed_ticker_file") or die "Coult not open $failed_ticker_file\n";
    foreach my $tmp_ticker (@failed_tickers) {
	print FTICKERS "$tmp_ticker\n";
    }
    close(FTICKERS);

    print LOGFILE "\n****************************************\n";

    my $full_ticker_count = scalar @tickers;
    my $valid_ticker_count = scalar @valid_tickers;
    my $failed_ticker_count = scalar @failed_tickers;

    print LOGFILE "Count of all tickers    = $full_ticker_count\n";
    print LOGFILE "Count of valid tickers  = $valid_ticker_count\n";
    print LOGFILE "Count of failed tickers = $failed_ticker_count\n\n";

    ##### Print end time to log
    my $endtime = localtime($^T);
    print LOGFILE "End time: $endtime\n";

    ##### Close Log
    print LOGFILE "\n****************************************\n";
    close(LOGFILE);

    ##### Email Log
    my $to = 'rswook@gmail.com';
    my $from = $to;
    my $subject = "Yahoo Historical Prices: $prod_date";
    
    my @logfile_lines = S_read_into_array($logfile);
    
    open(MAIL, "|/usr/sbin/sendmail -t");
    
    print MAIL "To: $to\n";
    print MAIL "From: $from\n";
    print MAIL "Subject: $subject\n\n";
    
    print MAIL "\n";

    print MAIL "****************************************\n";
    print MAIL "* Yahoo Historical Prices: $prod_date\n";
    print MAIL "****************************************\n\n";

    print MAIL "Logfile: $logfile\n";
    print MAIL "Price File: $hist_price_file\n\n";

    print MAIL "Valid Ticker File: $valid_ticker_file\n";
    print MAIL "Failed Ticker File: $failed_ticker_file\n\n";

    print MAIL "Count of all tickers    = $full_ticker_count\n";
    print MAIL "Count of valid tickers  = $valid_ticker_count\n";
    print MAIL "Count of failed tickers = $failed_ticker_count\n\n";

    print MAIL "****************************************\n\n";

    close(MAIL);
}
